<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 06/11/2018
 * Time: 21:34
 */

namespace App\Controller;


use App\Model\Table\PlanosTable;
use App\Util\MessagesUtil;
use RestApi\Controller\ApiController;

/**
 * Class PlansController
 * @package App\Controller
 *
 * @property PlanosTable $Planos
 */
class PlansController extends ApiController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Planos');

        // ====
        // Demais opções
        // ====
        $this->response->type('json');
    }


    public function recommendeds()
    {
        $plans = $this->Planos->find('all')
            ->contain([
                'Segmentacao', 'Operadoras', 'Tipos','Abrangencias',
                'Grupos', 'Carencias', 'Redes', 'Avaliacoes',
                'PlanosAtendimentos', 'Precos', 'Precos.Vidas'
            ])
            ->limit(5)
            ->order('rand()')
            ->toArray();

        foreach($plans as $key => $plan){
            $price = 0;
            foreach ($plan['precos'] as $preco){
                foreach($preco['vidas'] as $vida){
                    if( $price == 0){
                        $price = $vida['valor'];
                    }else{
                        if($price > $vida['valor']){
                            $price = $vida['valor'];
                        }
                    }
                }
            }

            $plan['valor_base'] = $price;
        }


        $this->apiResponse['data'] = $plans;
        $this->apiResponse['message'] = MessagesUtil::SUCCESS;
    }

    public function populates()
    {
        $plans = $this->Planos->find('all')
            ->contain([
                'Segmentacao', 'Operadoras', 'Tipos','Abrangencias',
                'Grupos', 'Carencias', 'Redes', 'Avaliacoes',
                'PlanosAtendimentos', 'Precos', 'Precos.Vidas'
            ])
            ->limit(3)
            ->order('rand()')
            ->toArray();

        foreach($plans as $key => $plan){
            $price = 0;
            foreach ($plan['precos'] as $preco){
                foreach($preco['vidas'] as $vida){
                    if( $price == 0){
                        $price = $vida['valor'];
                    }else{
                        if($price > $vida['valor']){
                            $price = $vida['valor'];
                        }
                    }
                }
            }

            $plan['valor_base'] = $price;
        }


        $this->apiResponse['data'] = $plans;
        $this->apiResponse['message'] = MessagesUtil::SUCCESS;
    }
}