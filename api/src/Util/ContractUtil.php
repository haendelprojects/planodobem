<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 25/05/2018
 * Time: 12:10
 */

namespace Flow\Util;


class ContractUtil
{
    /**
     * Tipo de contrato recorrente,
     * Há uma carga em cada mês até o final do contrato
     */
    const RECURRENT = 'RECURRENT';
    /**
     * tipo de contrato avulso
     * há apensa uma carga, a primeira
     */
    const DETACHED = 'DETACHED';
    /**
     * O mesmo do avulso
     */
    const FREE = 'FREE';
    /**
     * Tipo de contrato, projeto!
     */
    const PROJECT = 'PROJECT';
}