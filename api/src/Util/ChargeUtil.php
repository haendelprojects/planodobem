<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 25/05/2018
 * Time: 12:05
 */

namespace Flow\Util;


class ChargeUtil
{
    /**
     * Charga mensal
     */
    const MONTH_CHARGE = 'MONTH_CHARGE';
    /**
     * Charga aditiva
     */
    const ADDITIVE_CHARGE = 'ADDITIVE_CHARGE';
    /**
     * Debito de carga devedora
     */
    const CHARGE_DEBIT = 'CHARGE_DEBIT';

    const VALIDATED_CHARGE = 'VALIDATED_CHARGE';

    const INVALIDATED_CHARGE = 'INVALIDATED_CHARGE';
}