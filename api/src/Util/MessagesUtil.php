<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 25/05/2018
 * Time: 12:05
 */

namespace App\Util;


class MessagesUtil
{

    /**
     * Verificar o tipo de usuario que está
     * tendno acessar o recurso.
     */
    const CHECK_TYPE_USER = 'CHECK_TYPE_USER';

    /**
     * Recurso não está disponivel para o acesso do usuário
     */
    const NOT_AVAILABLE_FOR_USER = 'NOT_AVAILABLE_FOR_USER';

    /**
     * Falha ao savar o dado
     */
    const FAIL_SAVE =  'FAIL_SAVE';

    /**
     * Confito de horario
     */
    const DATE_HOUR_CONFLIT = 'DATE_HOUR_CONFLIT';

    /**
     * Menssagem padrão de sucesso
     */
    const SUCCESS = 'SUCCESS';

    /**
     * Dado não encontrado
     */
    const NOT_FOUND = 'NOT_FOUND';

    /**
     * Data minima de diferença é invalida
     */
    const MIN_DATE_INVALID = 'MIN_DATE_INVALID';
    /**
     * Objeto não encontrado
     * Dado já existe
     */
    const ALREADY_EXISTS = 'ALREADY_EXISTS';

    /**
     */
    const OBJECT_CREATED = 'OBJECT_CREATED';

    const FAIL_CHECKOUT = 'FAIL_CHECKOUT';

    /**
     * 'Não é possivel mudar o status!'
     */
    const NOT_CHANGE_STATUS = 'NOT_CHANGE_STATUS';
    const INVALID_EMAIL = 'INVALID_EMAIL';
    const INVALID_PASSWORD = 'INVALID_PASSWORD';
    /**
     * Não é permitido a ação por conta do status atual
     */
    const INVALID_STATUS_PERMISSION = 'INVALID_STATUS_PERMISSION';


}