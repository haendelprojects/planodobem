<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 11/05/2018
 * Time: 17:53
 */

namespace Flow\Util;

class StatusUtil
{
    /*
     * Aberto
     */
    const OPENED = 'OPENED';
    /**
     * Aceito
     */
    const ACCEPTED = 'ACCEPTED';
    /**
     * A caminho
     */
    const GOING = 'GOING';
    /**
     * Em serviço
     */
    const ON_SERVICE = 'ON_SERVICE';
    /**
     * Espera de pagamento
     */
    const AWAITING_PAYMENT = 'AWAITING_PAYMENT';
    /**
     * Espera de validação
     */
    const AWAITING_VALIDATION = 'AWAITING_VALIDATION';
    /**
     * Pagamento Pendente para o tecnico
     */
    const PENDING_PAYMENT = 'PENDING_PAYMENT';
    /**
     * Finalizado
     */
    const FINISHED = 'FINISHED';
    /**
     * Cancelado
     */
    const CANCELED = 'CANCELED';
    /**
     * IMprodutivo
     */
    const UNPRODUCTIVE = 'UNPRODUCTIVE';
    /**
     * Invalido
     */
    const INVALID = 'INVALID';
    /**
     * Rejeitado
     */
    const REJECTED = 'REJECTED';
    /**
     * Reagendamento
     */
    const RESCHEDULE = 'RESCHEDULE';
    /**
     * Validação da operação
     */
    const OPERATION_VALIDATION = 'OPERATION_VALIDATION';

    /**
     * Texto de significados dos status
     * @return array
     */
    public static function getListStatusText()
    {
        return [
            self::OPENED => 'Aberto',
            self::ACCEPTED => 'Aceito',
            self::GOING => 'A caminho',
            self::ON_SERVICE => 'Em atendimento',
            self::AWAITING_PAYMENT => 'Pagamento Pendente',
            self::PENDING_PAYMENT => 'Pagamento Pendente',
            self::AWAITING_VALIDATION => 'Espera de Validação',
            self::FINISHED => 'Finalizado',
            self::UNPRODUCTIVE => 'Improdutivo',
            self::INVALID => 'Invalido',
            self::OPERATION_VALIDATION => 'Validação da operação'
        ];
    }
}