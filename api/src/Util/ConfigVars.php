<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 25/05/2018
 * Time: 12:12
 */

namespace Flow\Util;

/**
 * Classe para valores de configuração padrões da findup
 * Class ConfigVars
 * @package Flow\Util
 */
class ConfigVars
{
    /**
     * Dias para validade do contrato
     */
    const DAYS_VALIDATED_CHARGE = 90;
}