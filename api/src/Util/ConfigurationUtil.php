<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 25/06/2018
 * Time: 14:59
 */

namespace Flow\Util;


class ConfigurationUtil
{
    /**
     * Notificação push quando o tecnico é desativado
     */
    const NOTIFICATION_PUSH_DISABLE_SPECIALIST = 'notification_push_disable_specialist';
    // Notificação push quando o chamadoé abnerto
    const NOTIFICATION_PUSH_OCCURRENCE_OPENED = 'notification_push_occurrence_opened';
    // Notificação push quando o chamado é finalizado
    const NOTIFICATION_PUSH_OCCURRENCE_FINISHED = 'notification_push_occurrence_finished';
    // Notificação push quando o chamado tem avaliação baixa
    const NOTIFICATION_PUSH_OCCURRENCE_LOW_VALIDATED  = 'notification_push_low_validated';
    // Notificação quando a um novo técnico na plataforma
    const NOTIFICATION_PUSH_NEW_SPECIALIST = 'notification_push_new_specialist';
}