<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Carencias Model
 *
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\HasMany $Planos
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\BelongsToMany $Planos
 *
 * @method \App\Model\Entity\Carencia get($primaryKey, $options = [])
 * @method \App\Model\Entity\Carencia newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Carencia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Carencia|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Carencia|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Carencia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Carencia[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Carencia findOrCreate($search, callable $callback = null, $options = [])
 */
class CarenciasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('carencias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Planos', [
            'foreignKey' => 'carencia_id'
        ]);
        $this->belongsToMany('Planos', [
            'foreignKey' => 'carencia_id',
            'targetForeignKey' => 'plano_id',
            'joinTable' => 'carencias_planos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->allowEmpty('nome');

        $validator
            ->integer('dias')
            ->allowEmpty('dias');

        return $validator;
    }
}
