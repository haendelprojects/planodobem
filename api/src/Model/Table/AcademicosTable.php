<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Academicos Model
 *
 * @property \App\Model\Table\AcoesTable|\Cake\ORM\Association\BelongsToMany $Acoes
 *
 * @method \App\Model\Entity\Academico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Academico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Academico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Academico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Academico|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Academico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Academico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Academico findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AcademicosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('academicos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Acoes', [
            'foreignKey' => 'academico_id',
            'targetForeignKey' => 'aco_id',
            'joinTable' => 'acoes_academicos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->allowEmpty('nome');

        $validator
            ->scalar('pdb')
            ->maxLength('pdb', 50)
            ->allowEmpty('pdb');

        $validator
            ->date('modifed')
            ->allowEmpty('modifed');

        return $validator;
    }
}
