<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PlanosAtendimentos Model
 *
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\BelongsTo $Planos
 * @property \App\Model\Table\SegmentacaosTable|\Cake\ORM\Association\BelongsTo $Segmentacaos
 *
 * @method \App\Model\Entity\PlanosAtendimento get($primaryKey, $options = [])
 * @method \App\Model\Entity\PlanosAtendimento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PlanosAtendimento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PlanosAtendimento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlanosAtendimento|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlanosAtendimento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PlanosAtendimento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PlanosAtendimento findOrCreate($search, callable $callback = null, $options = [])
 */
class PlanosAtendimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('planos_atendimentos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Planos', [
            'foreignKey' => 'plano_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Segmentacaos', [
            'foreignKey' => 'segmentacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['plano_id'], 'Planos'));
        $rules->add($rules->existsIn(['segmentacao_id'], 'Segmentacaos'));

        return $rules;
    }
}
