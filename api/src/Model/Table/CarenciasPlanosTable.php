<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CarenciasPlanos Model
 *
 * @property \App\Model\Table\CarenciasTable|\Cake\ORM\Association\BelongsTo $Carencias
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\BelongsTo $Planos
 *
 * @method \App\Model\Entity\CarenciasPlano get($primaryKey, $options = [])
 * @method \App\Model\Entity\CarenciasPlano newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CarenciasPlano[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CarenciasPlano|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CarenciasPlano|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CarenciasPlano patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CarenciasPlano[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CarenciasPlano findOrCreate($search, callable $callback = null, $options = [])
 */
class CarenciasPlanosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('carencias_planos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Carencias', [
            'foreignKey' => 'carencia_id'
        ]);
        $this->belongsTo('Planos', [
            'foreignKey' => 'plano_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['carencia_id'], 'Carencias'));
        $rules->add($rules->existsIn(['plano_id'], 'Planos'));

        return $rules;
    }
}
