<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Segmentacao Model
 *
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\HasMany $Planos
 * @property \App\Model\Table\PlanosAtendimentosTable|\Cake\ORM\Association\HasMany $PlanosAtendimentos
 *
 * @method \App\Model\Entity\Segmentacao get($primaryKey, $options = [])
 * @method \App\Model\Entity\Segmentacao newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Segmentacao[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Segmentacao|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Segmentacao|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Segmentacao patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Segmentacao[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Segmentacao findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SegmentacaoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('segmentacao');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Planos', [
            'foreignKey' => 'segmentacao_id'
        ]);
        $this->hasMany('PlanosAtendimentos', [
            'foreignKey' => 'segmentacao_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('hospitalar')
            ->allowEmpty('hospitalar');

        $validator
            ->boolean('ambulatorial')
            ->allowEmpty('ambulatorial');

        $validator
            ->boolean('odontologia')
            ->allowEmpty('odontologia');

        $validator
            ->boolean('obstetricia')
            ->allowEmpty('obstetricia');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
