<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Planos Model
 *
 * @property \App\Model\Table\GruposTable|\Cake\ORM\Association\BelongsTo $Grupos
 * @property \App\Model\Table\CarenciasTable|\Cake\ORM\Association\BelongsTo $Carencias
 * @property \App\Model\Table\SegmentacaoTable|\Cake\ORM\Association\BelongsTo $Segmentacao
 * @property \App\Model\Table\OperadorasTable|\Cake\ORM\Association\BelongsTo $Operadoras
 * @property \App\Model\Table\TiposTable|\Cake\ORM\Association\BelongsTo $Tipos
 * @property \App\Model\Table\ComissoesTable|\Cake\ORM\Association\BelongsTo $Comissoes
 * @property \App\Model\Table\AbrangenciasTable|\Cake\ORM\Association\BelongsTo $Abrangencias
 * @property \App\Model\Table\AvaliacoesTable|\Cake\ORM\Association\HasMany $Avaliacoes
 * @property \App\Model\Table\PessoasTable|\Cake\ORM\Association\HasMany $Pessoas
 * @property \App\Model\Table\PlanosAtendimentosTable|\Cake\ORM\Association\HasMany $PlanosAtendimentos
 * @property \App\Model\Table\PrecosTable|\Cake\ORM\Association\HasMany $Precos
 * @property \App\Model\Table\CarenciasTable|\Cake\ORM\Association\BelongsToMany $Carencias
 * @property \App\Model\Table\GruposTable|\Cake\ORM\Association\BelongsToMany $Grupos
 * @property \App\Model\Table\RedesTable|\Cake\ORM\Association\BelongsToMany $Redes
 *
 * @method \App\Model\Entity\Plano get($primaryKey, $options = [])
 * @method \App\Model\Entity\Plano newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Plano[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Plano|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plano|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Plano patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Plano[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Plano findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlanosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('planos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Grupos', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Carencias', [
            'foreignKey' => 'carencia_id'
        ]);
        $this->belongsTo('Segmentacao', [
            'foreignKey' => 'segmentacao_id'
        ]);
        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Comissoes', [
            'foreignKey' => 'comissao_id'
        ]);
        $this->belongsTo('Abrangencias', [
            'foreignKey' => 'abrangencia_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Avaliacoes', [
            'foreignKey' => 'plano_id'
        ]);
        $this->hasMany('Pessoas', [
            'foreignKey' => 'plano_id'
        ]);
        $this->hasMany('PlanosAtendimentos', [
            'foreignKey' => 'plano_id'
        ]);
        $this->hasMany('Precos', [
            'foreignKey' => 'plano_id'
        ]);
        $this->belongsToMany('Carencias', [
            'foreignKey' => 'plano_id',
            'targetForeignKey' => 'carencia_id',
            'joinTable' => 'carencias_planos'
        ]);
        $this->belongsToMany('Grupos', [
            'foreignKey' => 'plano_id',
            'targetForeignKey' => 'grupo_id',
            'joinTable' => 'grupos_planos'
        ]);
        $this->belongsToMany('Redes', [
            'foreignKey' => 'plano_id',
            'targetForeignKey' => 'rede_id',
            'joinTable' => 'redes_planos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('codigo')
            ->maxLength('codigo', 45)
            ->allowEmpty('codigo');

        $validator
            ->scalar('codigo_ans')
            ->maxLength('codigo_ans', 45)
            ->allowEmpty('codigo_ans');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 200)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('descricao')
            ->allowEmpty('descricao');

        $validator
            ->scalar('contrato')
            ->allowEmpty('contrato');

        $validator
            ->boolean('cooparticipacao')
            ->requirePresence('cooparticipacao', 'create')
            ->notEmpty('cooparticipacao');

        $validator
            ->scalar('hospedagem')
            ->maxLength('hospedagem', 45)
            ->requirePresence('hospedagem', 'create')
            ->notEmpty('hospedagem');

        $validator
            ->numeric('taxa_adesao')
            ->allowEmpty('taxa_adesao');

        $validator
            ->allowEmpty('reembolso');

        $validator
            ->scalar('beneficios_adicionais')
            ->allowEmpty('beneficios_adicionais');

        $validator
            ->scalar('quem_pode_aderir')
            ->allowEmpty('quem_pode_aderir');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 200)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->allowEmpty('compra_carencia');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['grupo_id'], 'Grupos'));
        $rules->add($rules->existsIn(['carencia_id'], 'Carencias'));
        $rules->add($rules->existsIn(['segmentacao_id'], 'Segmentacao'));
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['comissao_id'], 'Comissoes'));
        $rules->add($rules->existsIn(['abrangencia_id'], 'Abrangencias'));

        return $rules;
    }
}
