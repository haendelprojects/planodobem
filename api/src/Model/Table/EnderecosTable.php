<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Enderecos Model
 *
 * @property \App\Model\Table\EmpresasTable|\Cake\ORM\Association\HasMany $Empresas
 * @property \App\Model\Table\OperadorasTable|\Cake\ORM\Association\HasMany $Operadoras
 * @property \App\Model\Table\PessoasTable|\Cake\ORM\Association\HasMany $Pessoas
 * @property \App\Model\Table\RedesTable|\Cake\ORM\Association\HasMany $Redes
 *
 * @method \App\Model\Entity\Endereco get($primaryKey, $options = [])
 * @method \App\Model\Entity\Endereco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Endereco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Endereco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Endereco|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Endereco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Endereco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Endereco findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EnderecosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('enderecos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Empresas', [
            'foreignKey' => 'endereco_id'
        ]);
        $this->hasMany('Operadoras', [
            'foreignKey' => 'endereco_id'
        ]);
        $this->hasMany('Pessoas', [
            'foreignKey' => 'endereco_id'
        ]);
        $this->hasMany('Redes', [
            'foreignKey' => 'endereco_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('logradouro')
            ->maxLength('logradouro', 100)
            ->allowEmpty('logradouro');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 10)
            ->allowEmpty('numero');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 100)
            ->allowEmpty('complemento');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 60)
            ->allowEmpty('bairro');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade', 45)
            ->allowEmpty('cidade');

        $validator
            ->scalar('estado')
            ->maxLength('estado', 2)
            ->allowEmpty('estado');

        $validator
            ->scalar('pais')
            ->maxLength('pais', 5)
            ->allowEmpty('pais');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 10)
            ->allowEmpty('cep');

        return $validator;
    }
}
