<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Acoes Model
 *
 * @property \App\Model\Table\AcademicosTable|\Cake\ORM\Association\BelongsToMany $Academicos
 * @property \App\Model\Table\DentistasTable|\Cake\ORM\Association\BelongsToMany $Dentistas
 * @property \App\Model\Table\VoluntariosTable|\Cake\ORM\Association\BelongsToMany $Voluntarios
 *
 * @method \App\Model\Entity\Aco get($primaryKey, $options = [])
 * @method \App\Model\Entity\Aco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Aco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Aco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Aco|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Aco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Aco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Aco findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AcoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('acoes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Academicos', [
            'foreignKey' => 'aco_id',
            'targetForeignKey' => 'academico_id',
            'joinTable' => 'acoes_academicos'
        ]);
        $this->belongsToMany('Dentistas', [
            'foreignKey' => 'aco_id',
            'targetForeignKey' => 'dentista_id',
            'joinTable' => 'acoes_dentistas'
        ]);
        $this->belongsToMany('Voluntarios', [
            'foreignKey' => 'aco_id',
            'targetForeignKey' => 'voluntario_id',
            'joinTable' => 'acoes_voluntarios'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 200)
            ->allowEmpty('nome');

        $validator
            ->integer('meta')
            ->allowEmpty('meta');

        $validator
            ->date('data_inicio')
            ->allowEmpty('data_inicio');

        $validator
            ->date('data_fim')
            ->allowEmpty('data_fim');

        $validator
            ->scalar('descricao')
            ->allowEmpty('descricao');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        $validator
            ->scalar('banner')
            ->maxLength('banner', 200)
            ->allowEmpty('banner');

        $validator
            ->scalar('como_foi')
            ->allowEmpty('como_foi');

        $validator
            ->scalar('parceria')
            ->allowEmpty('parceria');

        $validator
            ->integer('media_idade')
            ->allowEmpty('media_idade');

        $validator
            ->integer('prevencao_prevista')
            ->allowEmpty('prevencao_prevista');

        $validator
            ->integer('limpeza_prevista')
            ->allowEmpty('limpeza_prevista');

        $validator
            ->integer('urgencia_prevista')
            ->allowEmpty('urgencia_prevista');

        $validator
            ->integer('prevencao_realizada')
            ->allowEmpty('prevencao_realizada');

        $validator
            ->integer('limpeza_realizada')
            ->allowEmpty('limpeza_realizada');

        $validator
            ->integer('urgencia_realizada')
            ->allowEmpty('urgencia_realizada');

        return $validator;
    }
}
