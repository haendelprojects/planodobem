<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Interesses Model
 *
 * @method \App\Model\Entity\Interess get($primaryKey, $options = [])
 * @method \App\Model\Entity\Interess newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Interess[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Interess|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Interess|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Interess patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Interess[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Interess findOrCreate($search, callable $callback = null, $options = [])
 */
class InteressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('interesses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->allowEmpty('nome');

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 50)
            ->allowEmpty('tipo');

        $validator
            ->scalar('responsavel')
            ->maxLength('responsavel', 50)
            ->allowEmpty('responsavel');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 20)
            ->allowEmpty('telefone');

        $validator
            ->scalar('celular')
            ->maxLength('celular', 20)
            ->allowEmpty('celular');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('rg')
            ->maxLength('rg', 15)
            ->allowEmpty('rg');

        $validator
            ->scalar('data_nascimento')
            ->maxLength('data_nascimento', 15)
            ->allowEmpty('data_nascimento');

        $validator
            ->scalar('informacoes')
            ->allowEmpty('informacoes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
