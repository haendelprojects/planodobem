<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AcoesAcademicos Model
 *
 * @property \App\Model\Table\AcoesTable|\Cake\ORM\Association\BelongsTo $Acoes
 * @property \App\Model\Table\AcademicosTable|\Cake\ORM\Association\BelongsTo $Academicos
 *
 * @method \App\Model\Entity\AcoesAcademico get($primaryKey, $options = [])
 * @method \App\Model\Entity\AcoesAcademico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AcoesAcademico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AcoesAcademico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AcoesAcademico|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AcoesAcademico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AcoesAcademico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AcoesAcademico findOrCreate($search, callable $callback = null, $options = [])
 */
class AcoesAcademicosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('acoes_academicos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Acoes', [
            'foreignKey' => 'acao_id'
        ]);
        $this->belongsTo('Academicos', [
            'foreignKey' => 'academico_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['acao_id'], 'Acoes'));
        $rules->add($rules->existsIn(['academico_id'], 'Academicos'));

        return $rules;
    }
}
