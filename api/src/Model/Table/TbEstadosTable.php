<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbEstados Model
 *
 * @property \App\Model\Table\TbCidadesTable|\Cake\ORM\Association\HasMany $TbCidades
 *
 * @method \App\Model\Entity\TbEstado get($primaryKey, $options = [])
 * @method \App\Model\Entity\TbEstado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TbEstado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TbEstado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TbEstado|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TbEstado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TbEstado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TbEstado findOrCreate($search, callable $callback = null, $options = [])
 */
class TbEstadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tb_estados');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('TbCidades', [
            'foreignKey' => 'tb_estado_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('uf')
            ->maxLength('uf', 10)
            ->requirePresence('uf', 'create')
            ->notEmpty('uf');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 20)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
