<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Empresas Model
 *
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\BelongsTo $Planos
 * @property \App\Model\Table\EnderecosTable|\Cake\ORM\Association\BelongsTo $Enderecos
 *
 * @method \App\Model\Entity\Empresa get($primaryKey, $options = [])
 * @method \App\Model\Entity\Empresa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Empresa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Empresa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Empresa|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Empresa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Empresa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Empresa findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmpresasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('empresas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Planos', [
            'foreignKey' => 'plano_interesse_id'
        ]);
        $this->belongsTo('Enderecos', [
            'foreignKey' => 'endereco_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('razao_social')
            ->maxLength('razao_social', 250)
            ->requirePresence('razao_social', 'create')
            ->notEmpty('razao_social');

        $validator
            ->scalar('cnpj')
            ->maxLength('cnpj', 250)
            ->allowEmpty('cnpj');

        $validator
            ->scalar('inscricao_municipal')
            ->maxLength('inscricao_municipal', 250)
            ->allowEmpty('inscricao_municipal');

        $validator
            ->scalar('atividade_principal')
            ->maxLength('atividade_principal', 250)
            ->allowEmpty('atividade_principal');

        $validator
            ->scalar('pessoa_nome')
            ->maxLength('pessoa_nome', 250)
            ->requirePresence('pessoa_nome', 'create')
            ->notEmpty('pessoa_nome');

        $validator
            ->scalar('pessoa_funcao')
            ->maxLength('pessoa_funcao', 250)
            ->allowEmpty('pessoa_funcao');

        $validator
            ->scalar('pessoa_fone')
            ->maxLength('pessoa_fone', 250)
            ->allowEmpty('pessoa_fone');

        $validator
            ->scalar('pessoa_fax')
            ->maxLength('pessoa_fax', 250)
            ->allowEmpty('pessoa_fax');

        $validator
            ->scalar('pessoa_email')
            ->maxLength('pessoa_email', 250)
            ->requirePresence('pessoa_email', 'create')
            ->notEmpty('pessoa_email');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['plano_interesse_id'], 'Planos'));
        $rules->add($rules->existsIn(['endereco_id'], 'Enderecos'));

        return $rules;
    }
}
