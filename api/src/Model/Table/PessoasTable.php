<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pessoas Model
 *
 * @property \App\Model\Table\PessoasTable|\Cake\ORM\Association\BelongsTo $Pessoas
 * @property \App\Model\Table\PessoasTable|\Cake\ORM\Association\BelongsTo $Pessoas
 * @property \App\Model\Table\ContatosTable|\Cake\ORM\Association\BelongsTo $Contatos
 * @property \App\Model\Table\EnderecosTable|\Cake\ORM\Association\BelongsTo $Enderecos
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\BelongsTo $Planos
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Pessoa get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pessoa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pessoa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pessoa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pessoa|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pessoa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pessoa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pessoa findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PessoasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pessoas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pessoas', [
            'foreignKey' => 'dependente_id'
        ]);
        $this->belongsTo('Pessoas', [
            'foreignKey' => 'responsavel_id'
        ]);
        $this->belongsTo('Contatos', [
            'foreignKey' => 'contato_id'
        ]);
        $this->belongsTo('Enderecos', [
            'foreignKey' => 'endereco_id'
        ]);
        $this->belongsTo('Planos', [
            'foreignKey' => 'plano_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 45)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('cpf')
            ->maxLength('cpf', 45)
            ->allowEmpty('cpf');

        $validator
            ->date('data_nascimento')
            ->requirePresence('data_nascimento', 'create')
            ->notEmpty('data_nascimento');

        $validator
            ->scalar('rg')
            ->maxLength('rg', 45)
            ->allowEmpty('rg');

        $validator
            ->scalar('rg_orgao')
            ->maxLength('rg_orgao', 45)
            ->allowEmpty('rg_orgao');

        $validator
            ->scalar('rg_uf')
            ->maxLength('rg_uf', 45)
            ->allowEmpty('rg_uf');

        $validator
            ->scalar('estado_civil')
            ->maxLength('estado_civil', 45)
            ->allowEmpty('estado_civil');

        $validator
            ->scalar('codigo_cadastro')
            ->maxLength('codigo_cadastro', 45)
            ->allowEmpty('codigo_cadastro');

        $validator
            ->numeric('plano_valor_contratado')
            ->allowEmpty('plano_valor_contratado');

        $validator
            ->scalar('nome_mae')
            ->maxLength('nome_mae', 200)
            ->allowEmpty('nome_mae');

        $validator
            ->scalar('nome_pai')
            ->maxLength('nome_pai', 200)
            ->allowEmpty('nome_pai');

        $validator
            ->integer('plano_pontos')
            ->allowEmpty('plano_pontos');

        $validator
            ->scalar('tipo')
            ->allowEmpty('tipo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dependente_id'], 'Pessoas'));
        $rules->add($rules->existsIn(['responsavel_id'], 'Pessoas'));
        $rules->add($rules->existsIn(['contato_id'], 'Contatos'));
        $rules->add($rules->existsIn(['endereco_id'], 'Enderecos'));
        $rules->add($rules->existsIn(['plano_id'], 'Planos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
