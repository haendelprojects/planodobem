<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Operadoras Model
 *
 * @property \App\Model\Table\ContatosTable|\Cake\ORM\Association\BelongsTo $Contatos
 * @property \App\Model\Table\EnderecosTable|\Cake\ORM\Association\BelongsTo $Enderecos
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\HasMany $Planos
 *
 * @method \App\Model\Entity\Operadora get($primaryKey, $options = [])
 * @method \App\Model\Entity\Operadora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Operadora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Operadora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Operadora|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Operadora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Operadora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Operadora findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OperadorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('operadoras');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Contatos', [
            'foreignKey' => 'contato_id'
        ]);
        $this->belongsTo('Enderecos', [
            'foreignKey' => 'endereco_id'
        ]);
        $this->hasMany('Planos', [
            'foreignKey' => 'operadora_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('registro_ans')
            ->allowEmpty('registro_ans')
            ->add('registro_ans', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('cnpj')
            ->maxLength('cnpj', 20)
            ->allowEmpty('cnpj');

        $validator
            ->scalar('razao_social')
            ->maxLength('razao_social', 45)
            ->allowEmpty('razao_social');

        $validator
            ->scalar('contrato')
            ->maxLength('contrato', 250)
            ->allowEmpty('contrato');

        $validator
            ->scalar('nome_fantasia')
            ->maxLength('nome_fantasia', 45)
            ->allowEmpty('nome_fantasia');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 50)
            ->allowEmpty('slug');

        $validator
            ->scalar('logo')
            ->maxLength('logo', 250)
            ->allowEmpty('logo');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        $validator
            ->boolean('compra_carencia')
            ->allowEmpty('compra_carencia');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['registro_ans']));
        $rules->add($rules->existsIn(['contato_id'], 'Contatos'));
        $rules->add($rules->existsIn(['endereco_id'], 'Enderecos'));

        return $rules;
    }
}
