<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Comissoes Model
 *
 * @method \App\Model\Entity\Comisso get($primaryKey, $options = [])
 * @method \App\Model\Entity\Comisso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Comisso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Comisso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Comisso|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Comisso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Comisso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Comisso findOrCreate($search, callable $callback = null, $options = [])
 */
class ComissoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('comissoes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('parcela_1')
            ->requirePresence('parcela_1', 'create')
            ->notEmpty('parcela_1');

        $validator
            ->numeric('parcela_2')
            ->requirePresence('parcela_2', 'create')
            ->notEmpty('parcela_2');

        $validator
            ->numeric('parcela_3')
            ->requirePresence('parcela_3', 'create')
            ->notEmpty('parcela_3');

        $validator
            ->numeric('parcela_4')
            ->requirePresence('parcela_4', 'create')
            ->notEmpty('parcela_4');

        $validator
            ->numeric('parcela_5')
            ->requirePresence('parcela_5', 'create')
            ->notEmpty('parcela_5');

        $validator
            ->numeric('parcela_6')
            ->requirePresence('parcela_6', 'create')
            ->notEmpty('parcela_6');

        return $validator;
    }
}
