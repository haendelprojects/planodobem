<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TbCidades Model
 *
 * @property \App\Model\Table\TbEstadosTable|\Cake\ORM\Association\BelongsTo $TbEstados
 *
 * @method \App\Model\Entity\TbCidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\TbCidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TbCidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TbCidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TbCidade|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TbCidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TbCidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TbCidade findOrCreate($search, callable $callback = null, $options = [])
 */
class TbCidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tb_cidades');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TbEstados', [
            'foreignKey' => 'tb_estado_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tb_estado_id'], 'TbEstados'));

        return $rules;
    }
}
