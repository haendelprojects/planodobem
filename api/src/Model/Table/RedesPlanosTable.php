<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RedesPlanos Model
 *
 * @property \App\Model\Table\PlanosTable|\Cake\ORM\Association\BelongsTo $Planos
 * @property \App\Model\Table\RedesTable|\Cake\ORM\Association\BelongsTo $Redes
 *
 * @method \App\Model\Entity\RedesPlano get($primaryKey, $options = [])
 * @method \App\Model\Entity\RedesPlano newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RedesPlano[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RedesPlano|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RedesPlano|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RedesPlano patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RedesPlano[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RedesPlano findOrCreate($search, callable $callback = null, $options = [])
 */
class RedesPlanosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('redes_planos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Planos', [
            'foreignKey' => 'plano_id'
        ]);
        $this->belongsTo('Redes', [
            'foreignKey' => 'rede_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['plano_id'], 'Planos'));
        $rules->add($rules->existsIn(['rede_id'], 'Redes'));

        return $rules;
    }
}
