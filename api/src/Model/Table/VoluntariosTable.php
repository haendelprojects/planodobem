<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Voluntarios Model
 *
 * @property \App\Model\Table\AcoesTable|\Cake\ORM\Association\BelongsToMany $Acoes
 *
 * @method \App\Model\Entity\Voluntario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Voluntario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Voluntario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Voluntario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Voluntario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Voluntario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Voluntario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Voluntario findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VoluntariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('voluntarios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Acoes', [
            'foreignKey' => 'voluntario_id',
            'targetForeignKey' => 'aco_id',
            'joinTable' => 'acoes_voluntarios'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->allowEmpty('nome');

        $validator
            ->date('modifed')
            ->allowEmpty('modifed');

        return $validator;
    }
}
