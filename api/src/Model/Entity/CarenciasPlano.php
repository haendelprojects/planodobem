<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CarenciasPlano Entity
 *
 * @property int $id
 * @property int $carencia_id
 * @property int $plano_id
 *
 * @property \App\Model\Entity\Carencia $carencia
 * @property \App\Model\Entity\Plano $plano
 */
class CarenciasPlano extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'carencia_id' => true,
        'plano_id' => true,
        'carencia' => true,
        'plano' => true
    ];
}
