<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Historico Entity
 *
 * @property int $id
 * @property string $descricao
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $compra_id
 * @property int $user_id
 * @property string $tipo
 * @property \Cake\I18n\FrozenTime $data_hora
 * @property int $executado
 *
 * @property \App\Model\Entity\Compra $compra
 * @property \App\Model\Entity\User $user
 */
class Historico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'created' => true,
        'modified' => true,
        'compra_id' => true,
        'user_id' => true,
        'tipo' => true,
        'data_hora' => true,
        'executado' => true,
        'compra' => true,
        'user' => true
    ];
}
