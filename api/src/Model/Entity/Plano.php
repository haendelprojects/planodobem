<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plano Entity
 *
 * @property int $id
 * @property string $codigo
 * @property string $codigo_ans
 * @property string $nome
 * @property string $descricao
 * @property string $contrato
 * @property int $grupo_id
 * @property bool $cooparticipacao
 * @property string $hospedagem
 * @property float $taxa_adesao
 * @property int $reembolso
 * @property string $beneficios_adicionais
 * @property string $quem_pode_aderir
 * @property string $slug
 * @property int $compra_carencia
 * @property int $carencia_id
 * @property bool $ativo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $segmentacao_id
 * @property int $operadora_id
 * @property int $tipo_id
 * @property int $comissao_id
 * @property int $abrangencia_id
 *
 * @property \App\Model\Entity\Grupo[] $grupos
 * @property \App\Model\Entity\Carencia[] $carencias
 * @property \App\Model\Entity\Segmentacao $segmentacao
 * @property \App\Model\Entity\Operadora $operadora
 * @property \App\Model\Entity\Tipo $tipo
 * @property \App\Model\Entity\Comisso $comisso
 * @property \App\Model\Entity\Abrangencia $abrangencia
 * @property \App\Model\Entity\Avaliaco[] $avaliacoes
 * @property \App\Model\Entity\Pessoa[] $pessoas
 * @property \App\Model\Entity\PlanosAtendimento[] $planos_atendimentos
 * @property \App\Model\Entity\Preco[] $precos
 * @property \App\Model\Entity\Rede[] $redes
 */
class Plano extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'codigo' => true,
        'codigo_ans' => true,
        'nome' => true,
        'descricao' => true,
        'contrato' => true,
        'grupo_id' => true,
        'cooparticipacao' => true,
        'hospedagem' => true,
        'taxa_adesao' => true,
        'reembolso' => true,
        'beneficios_adicionais' => true,
        'quem_pode_aderir' => true,
        'slug' => true,
        'compra_carencia' => true,
        'carencia_id' => true,
        'ativo' => true,
        'created' => true,
        'modified' => true,
        'segmentacao_id' => true,
        'operadora_id' => true,
        'tipo_id' => true,
        'comissao_id' => true,
        'abrangencia_id' => true,
        'grupos' => true,
        'carencias' => true,
        'segmentacao' => true,
        'operadora' => true,
        'tipo' => true,
        'comisso' => true,
        'abrangencia' => true,
        'avaliacoes' => true,
        'pessoas' => true,
        'planos_atendimentos' => true,
        'precos' => true,
        'redes' => true
    ];
}
