<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Segmentacao Entity
 *
 * @property int $id
 * @property bool $hospitalar
 * @property bool $ambulatorial
 * @property bool $odontologia
 * @property bool $obstetricia
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Plano[] $planos
 * @property \App\Model\Entity\PlanosAtendimento[] $planos_atendimentos
 */
class Segmentacao extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'hospitalar' => true,
        'ambulatorial' => true,
        'odontologia' => true,
        'obstetricia' => true,
        'modified' => true,
        'created' => true,
        'planos' => true,
        'planos_atendimentos' => true
    ];
}
