<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Operadora Entity
 *
 * @property int $id
 * @property int $registro_ans
 * @property string $cnpj
 * @property string $razao_social
 * @property string $contrato
 * @property string $nome_fantasia
 * @property int $contato_id
 * @property int $endereco_id
 * @property string $slug
 * @property string $logo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $ativo
 * @property bool $compra_carencia
 *
 * @property \App\Model\Entity\Contato $contato
 * @property \App\Model\Entity\Endereco $endereco
 * @property \App\Model\Entity\Plano[] $planos
 */
class Operadora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'registro_ans' => true,
        'cnpj' => true,
        'razao_social' => true,
        'contrato' => true,
        'nome_fantasia' => true,
        'contato_id' => true,
        'endereco_id' => true,
        'slug' => true,
        'logo' => true,
        'created' => true,
        'modified' => true,
        'ativo' => true,
        'compra_carencia' => true,
        'contato' => true,
        'endereco' => true,
        'planos' => true
    ];
}
