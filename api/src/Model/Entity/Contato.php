<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contato Entity
 *
 * @property int $id
 * @property string $ddd
 * @property string $telefone_1
 * @property string $telefone_2
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Operadora[] $operadoras
 * @property \App\Model\Entity\Pessoa[] $pessoas
 */
class Contato extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ddd' => true,
        'telefone_1' => true,
        'telefone_2' => true,
        'fax' => true,
        'email' => true,
        'website' => true,
        'facebook' => true,
        'twitter' => true,
        'created' => true,
        'modified' => true,
        'operadoras' => true,
        'pessoas' => true
    ];
}
