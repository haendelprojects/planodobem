<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Compra Entity
 *
 * @property int $id
 * @property int $titular_id
 * @property int $comprador_id
 * @property float $valor
 * @property int $total_pontos
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Pessoa $pessoa
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Extrato[] $extratos
 * @property \App\Model\Entity\Historico[] $historicos
 * @property \App\Model\Entity\Ponto[] $pontos
 */
class Compra extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'titular_id' => true,
        'comprador_id' => true,
        'valor' => true,
        'total_pontos' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'pessoa' => true,
        'user' => true,
        'extratos' => true,
        'historicos' => true,
        'pontos' => true
    ];
}
