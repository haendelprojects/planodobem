<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Preco Entity
 *
 * @property int $id
 * @property int $idade_inicial
 * @property int $idade_final
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $plano_id
 *
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Vida[] $vidas
 */
class Preco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'idade_inicial' => true,
        'idade_final' => true,
        'created' => true,
        'modified' => true,
        'plano_id' => true,
        'plano' => true,
        'vidas' => true
    ];
}
