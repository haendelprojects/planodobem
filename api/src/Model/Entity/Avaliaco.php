<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Avaliaco Entity
 *
 * @property int $id
 * @property int $pontos
 * @property string $descricao
 * @property int $plano_id
 * @property int $cliente_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Pessoa $pessoa
 */
class Avaliaco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'pontos' => true,
        'descricao' => true,
        'plano_id' => true,
        'cliente_id' => true,
        'created' => true,
        'modified' => true,
        'plano' => true,
        'pessoa' => true
    ];
}
