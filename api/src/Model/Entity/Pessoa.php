<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pessoa Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $cpf
 * @property \Cake\I18n\FrozenDate $data_nascimento
 * @property string $rg
 * @property string $rg_orgao
 * @property string $rg_uf
 * @property string $estado_civil
 * @property string $codigo_cadastro
 * @property float $plano_valor_contratado
 * @property string $nome_mae
 * @property string $nome_pai
 * @property int $plano_pontos
 * @property string $tipo
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 * @property int $dependente_id
 * @property int $responsavel_id
 * @property int $contato_id
 * @property int $endereco_id
 * @property int $plano_id
 * @property int $user_id
 *
 * @property \App\Model\Entity\Pessoa $pessoa
 * @property \App\Model\Entity\Contato $contato
 * @property \App\Model\Entity\Endereco $endereco
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\User $user
 */
class Pessoa extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'cpf' => true,
        'data_nascimento' => true,
        'rg' => true,
        'rg_orgao' => true,
        'rg_uf' => true,
        'estado_civil' => true,
        'codigo_cadastro' => true,
        'plano_valor_contratado' => true,
        'nome_mae' => true,
        'nome_pai' => true,
        'plano_pontos' => true,
        'tipo' => true,
        'modified' => true,
        'created' => true,
        'dependente_id' => true,
        'responsavel_id' => true,
        'contato_id' => true,
        'endereco_id' => true,
        'plano_id' => true,
        'user_id' => true,
        'pessoa' => true,
        'contato' => true,
        'endereco' => true,
        'plano' => true,
        'user' => true
    ];
}
