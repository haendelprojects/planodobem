<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Extrato Entity
 *
 * @property int $id
 * @property float $valor
 * @property string $tipo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $compra_id
 * @property string $descricao
 *
 * @property \App\Model\Entity\Compra $compra
 */
class Extrato extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'valor' => true,
        'tipo' => true,
        'created' => true,
        'modified' => true,
        'compra_id' => true,
        'descricao' => true,
        'compra' => true
    ];
}
