<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AcoesAcademico Entity
 *
 * @property int $id
 * @property int $acao_id
 * @property int $academico_id
 *
 * @property \App\Model\Entity\Aco $aco
 * @property \App\Model\Entity\Academico $academico
 */
class AcoesAcademico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'acao_id' => true,
        'academico_id' => true,
        'aco' => true,
        'academico' => true
    ];
}
