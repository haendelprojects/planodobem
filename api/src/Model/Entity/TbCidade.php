<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TbCidade Entity
 *
 * @property int $id
 * @property int $tb_estado_id
 * @property string $nome
 *
 * @property \App\Model\Entity\TbEstado $tb_estado
 */
class TbCidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tb_estado_id' => true,
        'nome' => true,
        'tb_estado' => true
    ];
}
