<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ponto Entity
 *
 * @property int $id
 * @property int $pontos
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $compra_id
 * @property int $acao_id
 * @property string $tipo
 *
 * @property \App\Model\Entity\Compra $compra
 * @property \App\Model\Entity\Aco $aco
 */
class Ponto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'pontos' => true,
        'created' => true,
        'modified' => true,
        'compra_id' => true,
        'acao_id' => true,
        'tipo' => true,
        'compra' => true,
        'aco' => true
    ];
}
