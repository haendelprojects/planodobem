<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RedesPlano Entity
 *
 * @property int $id
 * @property int $plano_id
 * @property int $rede_id
 *
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Rede $rede
 */
class RedesPlano extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'plano_id' => true,
        'rede_id' => true,
        'plano' => true,
        'rede' => true
    ];
}
