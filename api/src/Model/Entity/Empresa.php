<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Empresa Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $cnpj
 * @property string $inscricao_municipal
 * @property string $atividade_principal
 * @property string $pessoa_nome
 * @property string $pessoa_funcao
 * @property string $pessoa_fone
 * @property string $pessoa_fax
 * @property string $pessoa_email
 * @property int $plano_interesse_id
 * @property int $endereco_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Endereco $endereco
 */
class Empresa extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'razao_social' => true,
        'cnpj' => true,
        'inscricao_municipal' => true,
        'atividade_principal' => true,
        'pessoa_nome' => true,
        'pessoa_funcao' => true,
        'pessoa_fone' => true,
        'pessoa_fax' => true,
        'pessoa_email' => true,
        'plano_interesse_id' => true,
        'endereco_id' => true,
        'created' => true,
        'plano' => true,
        'endereco' => true
    ];
}
