<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vida Entity
 *
 * @property int $id
 * @property string $quantidade
 * @property float $valor
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $preco_id
 *
 * @property \App\Model\Entity\Preco $preco
 */
class Vida extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quantidade' => true,
        'valor' => true,
        'created' => true,
        'modified' => true,
        'preco_id' => true,
        'preco' => true
    ];
}
