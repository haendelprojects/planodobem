<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dentista Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $cro
 * @property \Cake\I18n\FrozenDate $created
 * @property \Cake\I18n\FrozenDate $modifed
 *
 * @property \App\Model\Entity\Aco[] $acoes
 */
class Dentista extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'cro' => true,
        'created' => true,
        'modifed' => true,
        'acoes' => true
    ];
}
