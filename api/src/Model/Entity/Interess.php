<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Interess Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $tipo
 * @property string $responsavel
 * @property string $telefone
 * @property string $celular
 * @property string $email
 * @property string $rg
 * @property string $data_nascimento
 * @property string $informacoes
 */
class Interess extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'tipo' => true,
        'responsavel' => true,
        'telefone' => true,
        'celular' => true,
        'email' => true,
        'rg' => true,
        'data_nascimento' => true,
        'informacoes' => true
    ];
}
