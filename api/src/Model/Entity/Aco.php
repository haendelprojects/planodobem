<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Aco Entity
 *
 * @property int $id
 * @property string $nome
 * @property \Cake\I18n\FrozenTime $created
 * @property int $meta
 * @property \Cake\I18n\FrozenDate $data_inicio
 * @property \Cake\I18n\FrozenDate $data_fim
 * @property string $descricao
 * @property bool $ativo
 * @property string $banner
 * @property string $como_foi
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $parceria
 * @property int $media_idade
 * @property int $prevencao_prevista
 * @property int $limpeza_prevista
 * @property int $urgencia_prevista
 * @property int $prevencao_realizada
 * @property int $limpeza_realizada
 * @property int $urgencia_realizada
 *
 * @property \App\Model\Entity\Academico[] $academicos
 * @property \App\Model\Entity\Dentista[] $dentistas
 * @property \App\Model\Entity\Voluntario[] $voluntarios
 */
class Aco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'created' => true,
        'meta' => true,
        'data_inicio' => true,
        'data_fim' => true,
        'descricao' => true,
        'ativo' => true,
        'banner' => true,
        'como_foi' => true,
        'modified' => true,
        'parceria' => true,
        'media_idade' => true,
        'prevencao_prevista' => true,
        'limpeza_prevista' => true,
        'urgencia_prevista' => true,
        'prevencao_realizada' => true,
        'limpeza_realizada' => true,
        'urgencia_realizada' => true,
        'academicos' => true,
        'dentistas' => true,
        'voluntarios' => true
    ];
}
