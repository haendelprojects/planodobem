<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlanosAtendimento Entity
 *
 * @property int $id
 * @property int $plano_id
 * @property int $segmentacao_id
 *
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Segmentacao $segmentacao
 */
class PlanosAtendimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'plano_id' => true,
        'segmentacao_id' => true,
        'plano' => true,
        'segmentacao' => true
    ];
}
