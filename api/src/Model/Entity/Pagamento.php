<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pagamento Entity
 *
 * @property int $id
 * @property string $valor
 * @property string $parcela
 * @property \Cake\I18n\FrozenDate $data
 * @property int $cliente_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Pessoa $pessoa
 */
class Pagamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'valor' => true,
        'parcela' => true,
        'data' => true,
        'cliente_id' => true,
        'created' => true,
        'modified' => true,
        'pessoa' => true
    ];
}
