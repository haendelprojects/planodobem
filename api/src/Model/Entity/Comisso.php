<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Comisso Entity
 *
 * @property int $id
 * @property float $parcela_1
 * @property float $parcela_2
 * @property float $parcela_3
 * @property float $parcela_4
 * @property float $parcela_5
 * @property float $parcela_6
 */
class Comisso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parcela_1' => true,
        'parcela_2' => true,
        'parcela_3' => true,
        'parcela_4' => true,
        'parcela_5' => true,
        'parcela_6' => true
    ];
}
