<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcoesVoluntariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcoesVoluntariosTable Test Case
 */
class AcoesVoluntariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AcoesVoluntariosTable
     */
    public $AcoesVoluntarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acoes_voluntarios',
        'app.acoes',
        'app.voluntarios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AcoesVoluntarios') ? [] : ['className' => AcoesVoluntariosTable::class];
        $this->AcoesVoluntarios = TableRegistry::getTableLocator()->get('AcoesVoluntarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AcoesVoluntarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
