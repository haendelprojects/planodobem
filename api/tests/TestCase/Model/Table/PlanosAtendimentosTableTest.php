<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlanosAtendimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlanosAtendimentosTable Test Case
 */
class PlanosAtendimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlanosAtendimentosTable
     */
    public $PlanosAtendimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.planos_atendimentos',
        'app.planos',
        'app.segmentacaos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PlanosAtendimentos') ? [] : ['className' => PlanosAtendimentosTable::class];
        $this->PlanosAtendimentos = TableRegistry::getTableLocator()->get('PlanosAtendimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PlanosAtendimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
