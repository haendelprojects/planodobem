<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VidasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VidasTable Test Case
 */
class VidasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VidasTable
     */
    public $Vidas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vidas',
        'app.precos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Vidas') ? [] : ['className' => VidasTable::class];
        $this->Vidas = TableRegistry::getTableLocator()->get('Vidas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Vidas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
