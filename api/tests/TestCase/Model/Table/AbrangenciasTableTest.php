<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AbrangenciasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AbrangenciasTable Test Case
 */
class AbrangenciasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AbrangenciasTable
     */
    public $Abrangencias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.abrangencias',
        'app.planos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Abrangencias') ? [] : ['className' => AbrangenciasTable::class];
        $this->Abrangencias = TableRegistry::getTableLocator()->get('Abrangencias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Abrangencias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
