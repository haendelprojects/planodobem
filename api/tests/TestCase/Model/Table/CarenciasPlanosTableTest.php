<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarenciasPlanosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarenciasPlanosTable Test Case
 */
class CarenciasPlanosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CarenciasPlanosTable
     */
    public $CarenciasPlanos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.carencias_planos',
        'app.carencias',
        'app.planos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CarenciasPlanos') ? [] : ['className' => CarenciasPlanosTable::class];
        $this->CarenciasPlanos = TableRegistry::getTableLocator()->get('CarenciasPlanos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CarenciasPlanos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
