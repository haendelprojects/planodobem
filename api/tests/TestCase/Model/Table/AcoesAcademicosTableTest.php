<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcoesAcademicosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcoesAcademicosTable Test Case
 */
class AcoesAcademicosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AcoesAcademicosTable
     */
    public $AcoesAcademicos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acoes_academicos',
        'app.acoes',
        'app.academicos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AcoesAcademicos') ? [] : ['className' => AcoesAcademicosTable::class];
        $this->AcoesAcademicos = TableRegistry::getTableLocator()->get('AcoesAcademicos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AcoesAcademicos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
