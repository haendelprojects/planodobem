<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TbEstadosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TbEstadosTable Test Case
 */
class TbEstadosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TbEstadosTable
     */
    public $TbEstados;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tb_estados',
        'app.tb_cidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TbEstados') ? [] : ['className' => TbEstadosTable::class];
        $this->TbEstados = TableRegistry::getTableLocator()->get('TbEstados', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbEstados);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
