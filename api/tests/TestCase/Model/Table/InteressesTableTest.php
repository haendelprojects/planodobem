<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InteressesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InteressesTable Test Case
 */
class InteressesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InteressesTable
     */
    public $Interesses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.interesses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Interesses') ? [] : ['className' => InteressesTable::class];
        $this->Interesses = TableRegistry::getTableLocator()->get('Interesses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Interesses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
