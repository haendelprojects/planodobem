<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcoesDentistasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcoesDentistasTable Test Case
 */
class AcoesDentistasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AcoesDentistasTable
     */
    public $AcoesDentistas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acoes_dentistas',
        'app.acoes',
        'app.dentistas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AcoesDentistas') ? [] : ['className' => AcoesDentistasTable::class];
        $this->AcoesDentistas = TableRegistry::getTableLocator()->get('AcoesDentistas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AcoesDentistas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
