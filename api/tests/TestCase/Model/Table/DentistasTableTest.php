<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DentistasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DentistasTable Test Case
 */
class DentistasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DentistasTable
     */
    public $Dentistas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dentistas',
        'app.acoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Dentistas') ? [] : ['className' => DentistasTable::class];
        $this->Dentistas = TableRegistry::getTableLocator()->get('Dentistas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dentistas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
