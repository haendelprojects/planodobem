<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RedesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RedesTable Test Case
 */
class RedesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RedesTable
     */
    public $Redes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.redes',
        'app.enderecos',
        'app.planos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Redes') ? [] : ['className' => RedesTable::class];
        $this->Redes = TableRegistry::getTableLocator()->get('Redes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Redes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
