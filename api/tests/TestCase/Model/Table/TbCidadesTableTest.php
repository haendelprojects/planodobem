<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TbCidadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TbCidadesTable Test Case
 */
class TbCidadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TbCidadesTable
     */
    public $TbCidades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tb_cidades',
        'app.tb_estados'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TbCidades') ? [] : ['className' => TbCidadesTable::class];
        $this->TbCidades = TableRegistry::getTableLocator()->get('TbCidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TbCidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
