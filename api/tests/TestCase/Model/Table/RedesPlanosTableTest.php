<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RedesPlanosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RedesPlanosTable Test Case
 */
class RedesPlanosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RedesPlanosTable
     */
    public $RedesPlanos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.redes_planos',
        'app.planos',
        'app.redes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RedesPlanos') ? [] : ['className' => RedesPlanosTable::class];
        $this->RedesPlanos = TableRegistry::getTableLocator()->get('RedesPlanos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RedesPlanos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
