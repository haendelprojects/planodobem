<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GruposPlanosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GruposPlanosTable Test Case
 */
class GruposPlanosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GruposPlanosTable
     */
    public $GruposPlanos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.grupos_planos',
        'app.planos',
        'app.grupos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GruposPlanos') ? [] : ['className' => GruposPlanosTable::class];
        $this->GruposPlanos = TableRegistry::getTableLocator()->get('GruposPlanos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GruposPlanos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
