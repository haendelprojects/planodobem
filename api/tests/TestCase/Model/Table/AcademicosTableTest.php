<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcademicosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcademicosTable Test Case
 */
class AcademicosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AcademicosTable
     */
    public $Academicos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.academicos',
        'app.acoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Academicos') ? [] : ['className' => AcademicosTable::class];
        $this->Academicos = TableRegistry::getTableLocator()->get('Academicos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Academicos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
