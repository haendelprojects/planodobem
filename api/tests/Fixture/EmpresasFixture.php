<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EmpresasFixture
 *
 */
class EmpresasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'razao_social' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cnpj' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'inscricao_municipal' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'atividade_principal' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pessoa_nome' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pessoa_funcao' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pessoa_fone' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pessoa_fax' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pessoa_email' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => '0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'plano_interesse_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'endereco_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'endereco_id' => ['type' => 'index', 'columns' => ['endereco_id'], 'length' => []],
            'plano_interesse_id' => ['type' => 'index', 'columns' => ['plano_interesse_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_empresas_enderecos_id1' => ['type' => 'foreign', 'columns' => ['endereco_id'], 'references' => ['enderecos', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_empresas_planos_id1' => ['type' => 'foreign', 'columns' => ['plano_interesse_id'], 'references' => ['planos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'razao_social' => 'Lorem ipsum dolor sit amet',
                'cnpj' => 'Lorem ipsum dolor sit amet',
                'inscricao_municipal' => 'Lorem ipsum dolor sit amet',
                'atividade_principal' => 'Lorem ipsum dolor sit amet',
                'pessoa_nome' => 'Lorem ipsum dolor sit amet',
                'pessoa_funcao' => 'Lorem ipsum dolor sit amet',
                'pessoa_fone' => 'Lorem ipsum dolor sit amet',
                'pessoa_fax' => 'Lorem ipsum dolor sit amet',
                'pessoa_email' => 'Lorem ipsum dolor sit amet',
                'plano_interesse_id' => 1,
                'endereco_id' => 1,
                'created' => '2018-11-06 15:53:18'
            ],
        ];
        parent::init();
    }
}
