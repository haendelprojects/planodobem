<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcoesVoluntariosFixture
 *
 */
class AcoesVoluntariosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'acao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'voluntario_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'acao_id' => ['type' => 'index', 'columns' => ['acao_id'], 'length' => []],
            'voluntario_id' => ['type' => 'index', 'columns' => ['voluntario_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_acoes_voluntarios_acoes' => ['type' => 'foreign', 'columns' => ['acao_id'], 'references' => ['acoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_acoes_voluntarios_voluntarios' => ['type' => 'foreign', 'columns' => ['voluntario_id'], 'references' => ['voluntarios', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'acao_id' => 1,
                'voluntario_id' => 1
            ],
        ];
        parent::init();
    }
}
