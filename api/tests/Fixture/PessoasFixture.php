<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PessoasFixture
 *
 */
class PessoasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nome' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cpf' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'data_nascimento' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'rg' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rg_orgao' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rg_uf' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'estado_civil' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'codigo_cadastro' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'plano_valor_contratado' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'nome_mae' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nome_pai' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'plano_pontos' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo' => ['type' => 'string', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dependente_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'responsavel_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'contato_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'endereco_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plano_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_clientes_enderecos1_idx' => ['type' => 'index', 'columns' => ['endereco_id'], 'length' => []],
            'fk_clientes_contatos1_idx' => ['type' => 'index', 'columns' => ['contato_id'], 'length' => []],
            'fk_clientes_responsaveis1_idx' => ['type' => 'index', 'columns' => ['responsavel_id'], 'length' => []],
            'dependente_id' => ['type' => 'index', 'columns' => ['dependente_id'], 'length' => []],
            'plano_id' => ['type' => 'index', 'columns' => ['plano_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_pessoas_contatos' => ['type' => 'foreign', 'columns' => ['contato_id'], 'references' => ['contatos', 'id'], 'update' => 'setNull', 'delete' => 'setNull', 'length' => []],
            'FK_pessoas_enderecos' => ['type' => 'foreign', 'columns' => ['endereco_id'], 'references' => ['enderecos', 'id'], 'update' => 'setNull', 'delete' => 'setNull', 'length' => []],
            'FK_pessoas_pessoas' => ['type' => 'foreign', 'columns' => ['responsavel_id'], 'references' => ['pessoas', 'id'], 'update' => 'setNull', 'delete' => 'setNull', 'length' => []],
            'FK_pessoas_pessoas_2' => ['type' => 'foreign', 'columns' => ['dependente_id'], 'references' => ['pessoas', 'id'], 'update' => 'setNull', 'delete' => 'setNull', 'length' => []],
            'FK_pessoas_planos' => ['type' => 'foreign', 'columns' => ['plano_id'], 'references' => ['planos', 'id'], 'update' => 'setNull', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'nome' => 'Lorem ipsum dolor sit amet',
                'cpf' => 'Lorem ipsum dolor sit amet',
                'data_nascimento' => '2018-11-06',
                'rg' => 'Lorem ipsum dolor sit amet',
                'rg_orgao' => 'Lorem ipsum dolor sit amet',
                'rg_uf' => 'Lorem ipsum dolor sit amet',
                'estado_civil' => 'Lorem ipsum dolor sit amet',
                'codigo_cadastro' => 'Lorem ipsum dolor sit amet',
                'plano_valor_contratado' => 1,
                'nome_mae' => 'Lorem ipsum dolor sit amet',
                'nome_pai' => 'Lorem ipsum dolor sit amet',
                'plano_pontos' => 1,
                'tipo' => 'Lorem ipsum dolor sit amet',
                'modified' => '2018-11-06 15:53:29',
                'created' => '2018-11-06 15:53:29',
                'dependente_id' => 1,
                'responsavel_id' => 1,
                'contato_id' => 1,
                'endereco_id' => 1,
                'plano_id' => 1,
                'user_id' => 1
            ],
        ];
        parent::init();
    }
}
